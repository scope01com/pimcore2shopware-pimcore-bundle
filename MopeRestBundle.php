<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use Mope\RestBundle\Tools\Installer;
use Pimcore\Extension\Bundle\Traits\PackageVersionTrait;

class MopeRestBundle extends AbstractPimcoreBundle
{
    use PackageVersionTrait;

    /**
     * @return string
     */
    protected function getComposerPackageName(): string
    {
        return 'mope/restBundle';
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return '1.5.0';
    }

    /**
     * @inheritDoc
     */
    public function getMinVersion()
    {
        return '6.1.2';
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return 'Provides REST Api to retrieve pimcore content';
    }

    /**
     * @return array
     */
    public function getCssPaths()
    {
        return [
            '/bundles/moperest/css/restbackend.css',
        ];
    }

    /**
     * @return Installer|object|\Pimcore\Extension\Bundle\Installer\InstallerInterface
     */
    public function getInstaller()
    {
        return $this->container->get(Installer::class);
    }
}
