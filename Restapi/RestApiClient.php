<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Restapi;

use Mope\RestBundle\Exception\RestBundleApiException;

/**
  * Simple Rest Api Client
  */
class RestApiClient
{

    /**
     * Method get
     */
    public const METHODE_GET    = 'GET';

    /**
     * Method put
     */
    public const METHODE_PUT    = 'PUT';

    /**
     * Method post
     */
    public const METHODE_POST   = 'POST';

    /**
     * Method delete
     */
    public const METHODE_DELETE = 'DELETE';

    /**
     * Valid Methods
     */
    protected $validMethods = [
        self::METHODE_GET,
        self::METHODE_PUT,
        self::METHODE_POST,
        self::METHODE_DELETE
    ];

    /**
     * Api url
     */
    protected $apiUrl;

    /**
     * Curl object
     */
    protected $cURL;

    /**
     * Constructor method
     *
     * RestApiClient constructor.
     * @param string $url
     * @param string $user
     * @param string $key
     * @throws \Exception
     */
    public function __construct($url, $user, $key)
    {
        $apiUrl = $url;
        $username = $user;
        $apiKey = $key;

        if ($apiUrl === '' || $username === '' || $apiKey === '') {
            throw new RestBundleApiException('configure rest plugin');
        }

        $this->apiUrl = rtrim($apiUrl, '/') . '/';
        //Initializes the cURL instance
        $this->cURL = \curl_init();
        \curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($this->cURL, CURLOPT_FOLLOWLOCATION, false);
        \curl_setopt($this->cURL, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        \curl_setopt($this->cURL, CURLOPT_USERPWD, $username . ':' . $apiKey);
        \curl_setopt($this->cURL, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json; charset=utf-8',
        ]);
    }

    /**
     * Method call
     *
     * @param string $url
     * @param string $method
     * @param array $data
     * @param array $params
     * @return mixed|void
     * @throws RestBundleApiException
     */
    public function call($url, $method = self::METHODE_GET, $data = [], $params = [])
    {
        if (!\in_array($method, $this->validMethods, true)) {
            throw new RestBundleApiException('Invalid HTTP-Methode: ' . $method);
        }
        $queryString = '';
        if (!empty($params)) {
            $queryString = \http_build_query($params);
        }
        $url = \rtrim($url, '?') . '?';
        $url = $this->apiUrl . $url . $queryString;
        $dataString = \json_encode($data);
        \curl_setopt($this->cURL, CURLOPT_URL, $url);
        \curl_setopt($this->cURL, CURLOPT_CUSTOMREQUEST, $method);
        \curl_setopt($this->cURL, CURLOPT_POSTFIELDS, $dataString);
        $result   = \curl_exec($this->cURL);
        return $this->prepareResponse($result);
    }

    /**
     * Method get
     *
     * @param string $url
     * @param array $params
     * @return mixed|void
     * @throws RestBundleApiException
     */
    public function get($url, $params = [])
    {
        return $this->call($url, self::METHODE_GET, [], $params);
    }

    /**
     * Method post
     *
     * @param string $url
     * @param array $data
     * @param array $params
     * @return mixed|void
     * @throws RestBundleApiException
     */
    public function post($url, $data = [], $params = [])
    {
        return $this->call($url, self::METHODE_POST, $data, $params);
    }

    /**
     * Method put
     *
     * @param string $url
     * @param array $data
     * @param array $params
     * @return mixed|void
     * @throws RestBundleApiException
     */
    public function put($url, $data = [], $params = [])
    {
        return $this->call($url, self::METHODE_PUT, $data, $params);
    }

    /**
     * Method delete
     *
     * @param string $url
     * @param array $params
     * @return mixed|void
     * @throws RestBundleApiException
     */
    public function delete($url, $params = [])
    {
        return $this->call($url, self::METHODE_DELETE, [], $params);
    }

    /**
     * Prepare response
     *
     * @param mixed $result
     * @return mixed|void
     * @throws RestBundleApiException
     */
    protected function prepareResponse($result)
    {
        if (null === $decodedResult = \json_decode($result, true)) {
            $jsonErrors = [
                JSON_ERROR_NONE => 'Es ist kein Fehler aufgetreten',
                JSON_ERROR_DEPTH => 'Die maximale Stacktiefe wurde erreicht',
                JSON_ERROR_CTRL_CHAR => 'Steuerzeichenfehler, möglicherweise fehlerhaft kodiert',
                JSON_ERROR_SYNTAX => 'Syntaxfehler',
            ];
            throw new RestBundleApiException('Could not decode json. json_last_error: ' . $jsonErrors[\json_last_error()] . ' RAW: ' . \print_r($result, true), 665);
        }
        if (!isset($decodedResult['success'])) {
            throw new RestBundleApiException('Invalid Response', 666);
        }
        if (!$decodedResult['success']) {
            throw new RestBundleApiException('No Success: ' . $decodedResult['message'], 667);
        }

        return $decodedResult;
    }
}
