<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\EventListener;

use Mope\RestBundle\Exception\RestBundleApiException;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Event\Model\DocumentEvent;
use Mope\RestBundle\Restapi\RestApiClient;
use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;
use Pimcore\Model\WebsiteSetting;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

/**
 * Class RestBundleListener
 * @package Mope\ShopwareRest\EventListener
 */
class RestBundleListener
{
    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var RestApiClient
     */
    private static $restClient;

    /**
     * RestBundleListener constructor.
     *
     * @param ContainerInterface $container
     * @throws \Exception
     */
    public function __construct(ContainerInterface $container) // <- Add this
    {
        $this->container = $container;
        /** @var  \Monolog\Logger $logger */
        $this->logger = new Logger('shopwareproductbundle');
        $this->logger->pushHandler(new StreamHandler(PIMCORE_LOG_DIRECTORY . '/moperestbundle.log'));

        $settingUrl = $settingUser = $settingToken = '';
        $setting = WebsiteSetting::getByName('shopware_api_url');
        if (null !== $setting) {
            $settingUrl = $setting->getData();
        }
        $setting = WebsiteSetting::getByName('shopware_api_user');
        if (null !== $setting) {
            $settingUser = $setting->getData();
        }
        $setting = WebsiteSetting::getByName('shopware_api_token');
        if (null !== $setting) {
            $settingToken = $setting->getData();
        }

        try {
            if ('' === (string)$settingUrl || '' === (string) $settingUser||'' === (string)$settingToken) {
                $settingUrl = $this->container->getParameter('moperestapiurl');
                $settingUser = $this->container->getParameter('moperestapiuser');
                $settingToken = $this->container->getParameter('moperestapitoken');
            }
        } catch (InvalidArgumentException $e) {
            $this->logger->error($e->getMessage());
        }

        if ('' === (string)$settingUrl || '' === (string) $settingUser||'' === (string)$settingToken) {
            $this->logger->error('no api settings found');
        } else {
            self::$restClient = new RestApiClient($settingUrl, $settingUser, $settingToken);
        }
    }

    /**
     * Execute stuff on pre update event
     *
     * @param ElementEventInterface $e
     */
    public function onPreUpdate(ElementEventInterface $e): void
    {
        if ($e instanceof DocumentEvent) {
            $obj = $e->getDocument();
            if ($obj instanceof \Pimcore\Model\Document\Printpage) {
                return;
            }
            $documentIds = [];
            $documentIds['cacheids'][] = $obj->getId();
            /** @var array $dep */
            $dependencies = $obj->getDependencies()->getRequiredBy();
            if (\is_array($dependencies) && \count($dependencies) > 0) {
                foreach ($dependencies as $dependency) {
                    if ($dependency["type"] === "document") {
                        $documentIds['cacheids'][] = (int)$dependency["id"];
                    }
                }
            }
            if (self::$restClient) {
                try {
                    $result = self::$restClient->put('webhooks/' . $obj->getId(), $documentIds);
                    $this->logger->info(print_r($documentIds, true));
                    $this->logger->info(print_r($result, true));
                } catch (RestBundleApiException $e) {
                    $this->logger->error($e->getMessage());
                }
            }
        }
    }

    /**
     * Execute stuff on pre add
     *
     * @param ElementEventInterface $e
     */
    public function onPreAdd(ElementEventInterface $e): void
    {
        // get controller setting
        try {
            $setController = $this->container->getParameter('moperestsetcontroller');
        } catch (InvalidArgumentException $e) {
            $this->logger->error($e->getMessage());
            $setController = false;
        }
        if ($e instanceof DocumentEvent) {
            /** @var Page $obj */
            $obj = $e->getDocument();
            // change default controller to rest content controller
            if (method_exists($obj, 'setController') && $obj->getController() === '' && $setController === true) {
                $obj->setController('@Mope\RestBundle\Controller\Frontend\ContentController');
            }
        }
    }
}
