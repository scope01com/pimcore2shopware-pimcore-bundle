###Installation
1. cd /pimcoreroot
2. composer require moreperform/restbundle
3. login into admin and klick on Tools -> Extensions
4. klick on enable and install
5. reload backend
6. activate api access in symtem settings
7. activate api in user settings and generate token

###Cache Invalidation
To invalidate cache define following parameters in parameters.yml
moperestapiurl: http://shopware.dev/api (shopware api url)
moperestapiuser: demo (shopware api user)
moperestapitoken: VAFJGTqqFkwzFu7zwZqv9gs4gotT01gggc0JnDT9 (shopware api token)
