<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Tools;

use Mope\RestBundle\MopeRestBundle;
use Pimcore\Extension\Bundle\Installer\AbstractInstaller;
use Pimcore\Extension\Bundle\PimcoreBundleManager;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Version;

/**
 * Class Installer
 * @package Mope\RestBundle\Tools
 */
class Installer extends AbstractInstaller
{
    /** @var bool|null */
    private $installed;
    /** @var MopeRestBundle  */
    private $bundle;

    /**
     * Installer constructor.
     * @param PimcoreBundleManager $bundleManager
     */
    public function __construct(PimcoreBundleManager $bundleManager)
    {
        parent::__construct();
        $this->bundle = $bundleManager->getActiveBundle(MopeRestBundle::class, false);
    }

    /**
     * install product bundle
     */
    public function install()
    {
        $this->createVersion();

        // reset installed state
        $this->installed = null;

        return true;
    }

    /**
     * uninstall product bundle
     *
     * @throws \Exception
     */
    public function uninstall()
    {
        // reset installed state
        $this->installed = null;
        return true;
    }


    /**
     * @return bool
     */
    public function canBeUninstalled()
    {
        return true;
    }

    /**
     *
     * @return bool
     */
    public function needsReloadAfterInstall()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function canBeInstalled()
    {
        $return = $this->_assertVersion();

        if ($return === true) {
            return !$this->isInstalled();
        }

        return $return;
    }

    /**
     * @inheritDoc
     */
    public function update()
    {
        $this->createVersion();
    }

    /**
     * Check pimcore and min version
     *
     * @return bool
     */
    private function _assertVersion()
    {
        $minVersion = $this->bundle->getMinVersion();
        $currentPimcoreversion = Version::getVersion();
        return !($minVersion > $currentPimcoreversion);
    }

    /**
     * Create new version
     */
    private function createVersion()
    {
        $version = $this->bundle->getVersion();
        $setting = WebsiteSetting::getByName("shopwarerest_version");
        if ($setting) {
            $setting->setValues(["name" => "shopwarerest_version", "data" => $version, "type" => "text"]);
            $setting->save();
        } else {
            $setting = new WebsiteSetting();
            $setting->setValues(["name" => "shopwarerest_version", "data" => $version, "type" => "text"]);
            $setting->save();
        }
    }

    /**
     * @inheritDoc
     */
    public function canBeUpdated()
    {
        $return = true;
        $currentVersion = $this->bundle->getVersion();

        $setting = WebsiteSetting::getByName('shopwarerest_version');
        if ($setting) {
            $installedVersion = $setting->getData();
        } else {
            $installedVersion = '';
        }
        if ($currentVersion === $installedVersion || $installedVersion === '') {
            $return = false;
        }

        if ($return === true) {
            $return = $this->_assertVersion();
        }

        return $return;
    }


    /**
     *  indicates if this bundle is currently installed
     *
     * @return bool
     */
    public function isInstalled()
    {
        if (null !== $this->installed) {
            return $this->installed;
        }

        $setting = WebsiteSetting::getByName('shopwarerest_version');
        if ($setting) {
            $this->installed = true;
        } else {
            $this->installed = false;
        }

        return $this->installed;
    }
}
