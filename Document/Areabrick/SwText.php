<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Document\Areabrick;

use Pimcore\Model\Document\Tag\Area\Info;

/**
 * Text brick
 * Class SwText
 * @package Mope\RestBundle\Document\Areabrick
 */
class SwText extends AbstractAreabrick
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'SW: Text';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Shopware text';
    }

    /**
     * @param Info $info
     * @return string
     */
    public function getHtmlTagOpen(Info $info)
    {
        return '';
    }

    /**
     * @param Info $info
     * @return string
     */
    public function getHtmlTagClose(Info $info)
    {
        return '';
    }

    /**
     * @return string
     */
    public function getTemplateLocation()
    {
        return static::TEMPLATE_LOCATION_BUNDLE;
    }
}
