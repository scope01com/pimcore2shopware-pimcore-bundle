<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Twig;

use Twig\Extension\AbstractExtension;

class ClassExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            'class' => new \Twig\TwigFunction('class', [$this, 'getClass'])
        ];
    }

    public function getName()
    {
        return 'class_twig_extension';
    }

    /**
     * @param $object
     * @return string
     * @throws \ReflectionException
     */
    public function getClass($object)
    {
        return (new \ReflectionClass($object))->getShortName();
    }
}
