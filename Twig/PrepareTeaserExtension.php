<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Twig;

use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Pimcore\Model\Document;

class PrepareTeaserExtension extends \Twig\Extension\AbstractExtension
{
    public function getFunctions()
    {
        return [
            'prepareteaser' => new \Twig\TwigFunction('prepareteaser', [$this, 'getTeaserData'])
        ];
    }

    public function getName()
    {
        return 'prepareteaser_twig_extension';
    }

    /**
     * @param Document $object
     * @return array
     */
    public function getTeaserData($object)
    {
        $dataResults = [];
        /** @var Document\Page $child */
        foreach ($object->getChildren() as $child) {
            if (\count($child->getElements()) > 0) {
                $tmpData = ['headline' => '', 'text' => '', 'image' => '', 'link' => ''];
                foreach ($child->getElements() as $element) {
                    if ($element instanceof \Pimcore\Model\Document\Tag\Input) {
                        if ('' === $tmpData['headline']) {
                            $tmpData['headline'] = $element->getData();
                        }
                    }
                    if ($element instanceof \Pimcore\Model\Document\Tag\Wysiwyg) {
                        if ('' === $tmpData['text']) {
                            $tmpData['text'] = \mb_substr(strip_tags($element->getData()), 0, 100);
                        }
                    }

                    if ($element instanceof \Pimcore\Model\Document\Tag\Image) {
                        if ('' === $tmpData['image']) {
                            $imageData = $element->getData();
                            /** @var Asset|null $asset */
                            $asset = Asset::getById($imageData['id']);
                            if (null !== $asset) {
                                $tmpData['image'] = \Pimcore\Tool::getHostUrl() . $asset->getRealPath() . \str_replace(
                                    ' ',
                                    '%20',
                                    $asset->getFilename()
                                    );
                            }
                        }
                    }


                    $tmpData['link'] =  $child->getFullPath();
                }
                $dataResults[$child->getId()] = $tmpData;
            }
        }

        return $dataResults;
    }
}
