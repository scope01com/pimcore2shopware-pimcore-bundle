<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Controller\Frontend;


use Symfony\Component\HttpFoundation\Request;

/**
 * Class ContentController
 * @package Mope\RestBundle\Controller\Frontend
 */
class ContentController extends FrontendController
{
    /**
     * Default action
     *
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {
    }
}
