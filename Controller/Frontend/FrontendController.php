<?php

declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Controller\Frontend;

use Pimcore\Controller\FrontendController as BaseFrontendController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

abstract class FrontendController extends BaseFrontendController
{
    /**
     * @inheritDoc
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        // enable twig view auto-rendering
        $this->setViewAutoRender($event->getRequest(), true, 'twig');
    }
}
