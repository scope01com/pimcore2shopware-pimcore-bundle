<?php declare(strict_types=1);
/**
 * Implemented by scope01 GmbH team https://scope01.com
 *
 * @copyright scope01 GmbH https://scope01.com
 * @license proprietär
 * @link https://scope01.com
 */

namespace Mope\RestBundle\Controller\Rest;

use Doctrine\DBAL\DBALException;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Db;
use Pimcore\Db\Connection;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model\Document\Listing;
use Pimcore\Model\Document\Page;
use Pimcore\Model\Document\Tag\Input;
use Pimcore\Model\Document\Tag\Wysiwyg;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Helper\Mail as MailHelper;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Pimcore\Model\Search\Backend\Data\Listing as SearchListing;
use Pimcore\Model\Element;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class WebContentController
 * @package Mope\RestBundle\Controller\Rest
 */
class WebContentController
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    private $debug = false;

    public const OBJECT_NOT_PUBLISHED = 100;

    public const ELEMENT_DOES_NOT_EXIST = -1;

    /**
     * WebContentController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @Method("POST")
     * @Route("/webcontent")
     *
     * @apiParam {json} data a new object data
     * @apiParam {string} apikey your access token
     *
     * @apiSuccess {json} success parameter of the returned data = true
     * @apiError {json} success parameter of the returned data = false
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function postAction(Request $request): JsonResponse
    {
        $this->checkApiToken($request);

        // enable debug if active
        if ($this->container->getParameter('kernel.debug')) {
            $this->debug = true;
        }

        $this->logdebug('request from: ' . $request->getClientIp() . ' to method: ' . __METHOD__);
        $this->logdebug('host: ' . $request->getHttpHost());
        $this->logdebug('request path: ' . $request->get('path'));

        $path = $request->get('path');
        $language = $request->get('language');
        $categoryId = $request->get('categoryId');
        $breadcrumb = [];

        // if path string
        if (!is_numeric($path)) {
            // get document by path
            $path = '/' . $language . $path;
            /** @var Document $document */
            $document = Document::getByPath($path);
        } else {
            // if path numeric
          /** @var Document $document */
            $document = Document::getById($path);
        }
        if ('' !== $language) {
            /** @var ContainerInterface $container */
            $container = \Pimcore::getKernel()->getContainer();
            $container->get('pimcore.locale')->setLocale($language);
        }
        // if loaded
        if (null !== $document) {
            $this->logdebug('loaded document: ' . $document->getId());
            // render document with all params
            $reqData = [];
            \parse_str($request->getContent(), $reqData);
            if (isset($reqData['objectid'])) {
                /** @var DataObject\Concrete $object */
                $object = DataObject::getById($reqData['objectid']);
                if ($object->isPublished() === false) {
                    throw $this->createNotFoundResponseException([
                        'msg' => sprintf('Document with path %d does not exist', $path),
                        'code' => static::ELEMENT_DOES_NOT_EXIST
                    ]);
                }
            }
            $content = $this->renderDocumentContent($document, $reqData);
            $this->logdebug('rendered document: ' . $content);
            $content = $this->convertUrls($content, $document, $language, $categoryId);

            $iStartPosition = strpos($content, '<!-- BEGIN:CONTENT -->') + 22;
            $iContentLength = strpos($content, '<!-- END:CONTENT -->') - $iStartPosition;

            if (strpos($content, '<!-- END:CONTENT -->') > 0) {
                $content = substr($content, $iStartPosition, $iContentLength);
            }
            $this->logdebug('final document: ' . $content);

            $metadata['custom'] = $document->getMetaData();
            $metadata = array_merge(
                $metadata,
                ['title' => $document->getTitle(), 'description' => $document->getDescription()]
            );
            $this->logdebug('metadata navigation: ' . print_r($metadata, true));
            $first = true;
            foreach (\array_reverse($document->getParentIds()) as $parentId) {
                if ($first === false) {
                    /** @var Document|null $loadedDoc */
                    $loadedDoc = Page::getById($parentId);
                    if($loadedDoc) {
                        $parentId = $loadedDoc->getParentId();
                        if ($parentId !== 1 && $parentId !== 0) {
                            $docName = $loadedDoc->getProperty('navigation_name');
                            $breadcrumb[] = [
                                'name' => $docName,
                                'link' => $loadedDoc->getFullPath(),
                                'active' => false
                            ];
                        }
                    }
                }
                $first = false;
            }
            $docName = $document->getProperty('navigation_name');
            $breadcrumb[] = ['name' => $docName, 'link' => $document->getFullPath(), 'active' => true];
            $this->logdebug('breadcrumb: ' . print_r($breadcrumb, true));
            return $this->createSuccessResponse([
                'content' => $content,
                'metadata' => $metadata,
                'breadcrumb' => $breadcrumb,
                'documentid' => $document->getId(),
                'products' => $request->getSession()->get('products')
            ]);
        }

        $this->logdebug('throw exception: ' . 'document with path' . $path . ' not found');

        throw $this->createNotFoundResponseException([
            'msg' => sprintf('Document with path %d does not exist', $path),
            'code' => static::ELEMENT_DOES_NOT_EXIST
        ]);
    }

    /**
     * @Method("GET")
     * @Route("/resolveobject")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ResponseException
     */
    public function resolveObjectAction(Request $request): JsonResponse
    {
        $this->checkApiToken($request);
        $objectId = $request->get('id');
        /** @var DataObject|null $object */
        $object = DataObject::getById($objectId);
        if (null !== $object) {
            return $this->createSuccessResponse([
                'objectid' => $object->getId(),
            ]);
        }
        return $this->createSuccessResponse([
            'objectid' => 0,
        ]);
    }

    /**
     * @Method("GET")
     * @Route("/documentselect")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function documentSelectAction(Request $request): JsonResponse
    {
        $this->checkApiToken($request);
        $listing = new Listing();
        $listing->setCondition('id != 1');
        $documentData = [];
        foreach ($listing as $child) {
            if ($child instanceof Page) {
                $documentData[] = [
                    'name' => $child->getProperty('navigation_name') ?: $child->getKey(),
                    'id' => $child->getId()
                ];
            }
        }
        return $this->createSuccessResponse([
            'data' => $documentData
        ]);
    }


    /**
     * Executes content search
     *
     * @Method("GET")
     * @Route("/search")
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws DBALException
     * @throws ResponseException
     */
    public function searchAction(Request $request): JsonResponse
    {
        $this->checkApiToken($request);
        $searchterm = $request->get('searchterm');
        $page = $request->get('page');
        $language = $request->get("language");
        $resultsperpage = $request->get('resultsperpage');
        $offset = ($page - 1) * $resultsperpage;

        $langPath = '/' . $language . '/';
        $objectCondition = '';
        if (class_exists('Pimcore\Model\DataObject\ShopwareProducts')) {
            // search in objects
            // example: ;i:1301;
            $searcherList = new SearchListing();
            $condition = "( MATCH (`data`,`properties`) AGAINST ('$searchterm' IN BOOLEAN MODE) ) AND ( maintype IN ('object') ) AND ( type IN ('object') ) AND ( subtype IN ('shopwareProducts') )";
            $searcherList->setCondition($condition);
            $hits = $searcherList->load();
            if (count($hits) > 0) {
                foreach ($hits as $hit) {
                    $element = Element\Service::getElementById($hit->getId()->getType(), $hit->getId()->getId());
                    $objectCondition .= 'OR docelems.data like "%;i:' . $element->getId() . ';%"';
                }
            }
        }

        $event = new GenericEvent($request, ['searchterm' => $searchterm]);
        $eventExec = \Pimcore::getEventDispatcher()->dispatch('rest.bundle.on.search.object.condition', $event);
        $objectCondition = $eventExec->getArgument('objectCondition');

        $searchSql = "SELECT DISTINCT docelems.documentId FROM documents_elements as docelems LEFT JOIN documents as doc ON docelems.documentId = doc.id
                      WHERE (docelems.type = 'input' OR docelems.type = 'wysiwyg' OR docelems.type = 'textarea' OR docelems.type = 'multihref')
                      AND (docelems.data LIKE '%$searchterm%' $objectCondition) AND doc.path LIKE '$langPath%'  LIMIT $resultsperpage OFFSET $offset;";

        $pagingSql = "SELECT DISTINCT docelems.documentId FROM documents_elements as docelems LEFT JOIN documents as doc ON docelems.documentId = doc.id
                      WHERE (docelems.type = 'input' OR docelems.type = 'wysiwyg' OR docelems.type = 'textarea' OR docelems.type = 'multihref')
                      AND (docelems.data LIKE '%$searchterm%' $objectCondition) AND doc.path LIKE '$langPath%'";
        /** @var Connection $db */
        $db = Db::get();
        $results = $db->query($searchSql)->fetchAll();
        $pagingResults = $db->query($pagingSql)->fetchAll();
        $pagingResultCount = count($pagingResults);
        $dataResults = [];

        foreach ($results as $docid) {
            $documentid = (int)$docid['documentId'];
            /** @var Page|null $loadedDocument */
            $loadedDocument = Document::getById($documentid);
            if ($loadedDocument) {
                $lements = $loadedDocument->getElements();

                $tmpData = [];
                foreach ($lements as $element) {
                    if ($element instanceof Input) {
                        if ((string)$tmpData['headline'] === '') {
                            $tmpData['headline'] = $element->getData();
                        }
                    }
                    if ($element instanceof Wysiwyg) {
                        if ((string)$tmpData['text'] === '') {
                            $tmpData['text'] = mb_substr(strip_tags($element->getData()), 0, 100);
                        }
                    }

                    $tmpData['link'] = str_replace('/' . $language . '/', '', $loadedDocument->getFullPath());
                }
                if ((isset($tmpData['headline']) && $tmpData['headline'] !== '') || (isset($tmpData['text']) && $tmpData['text'] !== '')) {
                    $dataResults[$documentid] = $tmpData;
                } else {
                    $tmpData['headline'] = $loadedDocument->getKey();
                    $dataResults[$documentid] = $tmpData;
                }
            }
        }

        $searchResults['queryString'] = $searchterm;
        $searchResults['count'] = count($results);
        $searchResults['pages'] = ceil($pagingResultCount / (int)$resultsperpage);
        $searchResults['page'] = (int)$page;
        $searchResults['resultsperpage'] = (int)$resultsperpage;
        $searchResults['result'] = $dataResults;

        return $this->createSuccessResponse([
            'result' => $searchResults
        ]);
    }

    /**
     * Gets pimcore navigation
     *
     * @Method("GET")
     * @Route("/navigation")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function navigationAction(Request $request): JsonResponse
    {
        $this->checkApiToken($request);
        // enable debug if active
        if ($this->container->getParameter('kernel.debug')) {
            $this->debug = true;
        }
        $categoryNavigation = $request->get("catnav");
        $docId = $request->get("documentid");
        $document = Document::getById($docId);
        $this->logdebug('request catnav: ' . $request->get("catnav"));

        /** @var \Pimcore\Twig\Extension\NavigationExtension $navigationExtension */
        $navigationExtension = $this->container->get('pimcore.twig.extension.navigation');
        $renderedNavigation = "";

        if ($categoryNavigation > 0) {
            $buildNavigation = $navigationExtension->buildNavigation(
                $document,
                Document::getById($categoryNavigation)
            );
            $renderOptions = [
                'ulClass' => 'shop-sites--navigation sidebar--navigation navigation--list is--drop-down is--level0',
                'addPageClassToLi' => true,
                'expandSiblingNodesOfActiveBranch' => true,
                'activeClass' => 'is--active'
            ];
            foreach ($buildNavigation->getPages() as $page) {
                $page->setClass('navigation--entry');
            }
            $renderedNavigation = $navigationExtension->render(
                $buildNavigation,
                'menu',
                'renderMenu',
                $renderOptions
            );

            $this->logdebug('rendered navigation: ' . $renderedNavigation);
        }
        return $this->createSuccessResponse([
            'navigation' => $renderedNavigation,
        ]);
    }

    /**
     * Gets pimcore navigation as data array
     *
     * @Method("GET")
     * @Route("/navigationdata")
     *
     * @return JsonResponse
     *
     * @param Request $request
     */
    public function navigationdataAction(Request $request): JsonResponse
    {
        $this->checkApiToken($request);
        /** @var \Pimcore\Twig\Extension\NavigationExtension $navigationExtension */
        $navigationExtension = $this->container->get('pimcore.twig.extension.navigation');
        $tree = [];
        $language = $request->get("language");
        $baseurl = $request->get("baseurl");
        $c = $request->get("c");
        $docId = $request->get("documentid");
        $activeDocId = $request->get("activedocumentid");
        $document = Document::getById($docId);
        if ($document) {
            $buildNavigation = $navigationExtension->buildNavigation(
                $document,
                $document
            );
            $tree = $this->buildTree($buildNavigation, $document->getId(), $language, $baseurl, $c, $activeDocId);
        }

        return $this->createSuccessResponse([
            'navigation' => $tree,
        ]);
    }


    /**
     * Gets pimcore object relation document data
     *
     * @Method("GET")
     * @Route("/relationdata")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws DBALException
     */
    public function relationdataAction(Request $request): JsonResponse
    {
        $this->checkApiToken($request);
        $documents = [];
        if (\class_exists('Pimcore\Model\DataObject\ShopwareProducts')) {
            $language = $request->get("language");
            $baseurl = $request->get("baseurl");
            $langPath = '/' . $language . '/';
            $productId = $request->get("productid");
            /** @var Connection $db */
            $db = Db::get();
            /** @var DataObject\ShopwareProducts|null $product */
            $product = DataObject\ShopwareProducts::getBySwidentifier($productId)->current();
            if (null !== $product) {
                $sql = "SELECT docelems.documentId, docelems.data FROM documents_elements as docelems LEFT JOIN documents as doc ON docelems.documentId = doc.id
                        WHERE docelems.type = 'multihref' AND docelems.name like '%.products'
                        AND doc.path LIKE '$langPath%'
                        AND data like '%;i:" . $product->getId() . ";%'";

                $results = $db->query($sql)->fetchAll();
                if (\count($results) > 0) {
                    foreach ($results as $result) {
                        $dataItems = \unserialize($result['data'], ['allowed_classes' => false]);
                        if (\count($dataItems) > 0) {
                            foreach ($dataItems as $dataItem) {
                                if ((int)$dataItem['id'] === (int)$product->getId()) {
                                    $docId = $result['documentId'];
                                    /** @var Page|null $loadedDocument */
                                    $loadedDocument = Document::getById($docId);
                                    if (null !== $loadedDocument) {
                                        $elements = $loadedDocument->getElements();

                                        $tmpData = [];
                                        $dataResults = [];
                                        foreach ($elements as $element) {
                                            if ($element instanceof Input) {
                                                if ('' === (string)$tmpData['headline']) {
                                                    $tmpData['headline'] = $element->getData();
                                                }
                                            }
                                            if ($element instanceof Wysiwyg) {
                                                if ('' === (string)$tmpData['text']) {
                                                    $tmpData['text'] = mb_substr(
                                                        strip_tags($element->getData()),
                                                        0,
                                                        100
                                                    );
                                                }
                                            }

                                            $tmpData['link'] = str_replace(
                                                '/' . $language . '/',
                                                '',
                                                $loadedDocument->getFullPath()
                                            );
                                            $tmpData['link'] = $baseurl . '/' . $tmpData['link'];
                                        }
                                        if ((isset($tmpData['headline']) && mb_strlen($tmpData['headline']) > 0) || (isset($tmpData['text']) && mb_strlen($tmpData['text']) > 0)) {
                                            $dataResults[$docId] = $tmpData;
                                        } else {
                                            $tmpData['headline'] = $loadedDocument->getKey();
                                            $dataResults[$docId] = $tmpData;
                                        }
                                        $documents[] = [
                                            'documentid' => $result['documentId'],
                                            'content' => $dataResults
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $event = new GenericEvent($request, ['documents' => $documents]);
        $eventExec = \Pimcore::getEventDispatcher()->dispatch('rest.bundle.on.relation.data', $event);
        $documents = $eventExec->getArgument('documents');

        return $this->createSuccessResponse([
            'relationdata' => $documents,
        ]);
    }

    /**
     * Renders document
     *
     * @param $doc
     * @param array $params
     * @return string $content
     */
    public function renderDocumentContent($doc, $params = []): string
    {
        $useLayout = true;
        return Document\Service::render($doc, $params, $useLayout);
    }


    /**
     * Log messages if debug is active
     *
     * @param $message
     * @throws \Exception
     */
    private function logdebug($message): void
    {
        /** @var \Monolog\Logger $logger */
        $logger = new Logger('moperestbundle');
        $logger->pushHandler(new StreamHandler(PIMCORE_LOG_DIRECTORY . '/moperestbundle.log'));
        if ($this->debug === true) {
            $logger->debug($message);
        }
    }

    /**
     * Builds tree for navigation
     *
     * @param array|\Countable $itemList
     * @param int $parentId
     * @return array|null
     */
    public function buildTree($itemList, $parentId, $language, $baseurl, $c, $activeDocId): ?array
    {
        // return an array of items with parent = $parentId
        $result = [];
        if (\count($itemList) > 0) {
            foreach ($itemList as $item) {
                if ($item instanceof \Pimcore\Navigation\Page\Document) {
                    $itemObj = $item->getDocument();
                } else {
                    $itemObj = $item;
                }
                if ($itemObj->getProperty('navigation_exclude') === true) {
                    continue;
                }
                if ($itemObj->getParentId() === $parentId) {
                    $itemData['id'] = $itemObj->getId();
                    $itemData['name'] = $itemObj->getProperty('navigation_name');
                    ;
                    $link = \str_replace('/' . $language, '', $itemObj->getFullPath());
                    $itemData['link'] = $baseurl . $link . '?c=' . $c;
                    $itemData['parent'] = $itemObj->getParentId();
                    if ((int)$activeDocId === $itemObj->getId()) {
                        $active = true;
                    } else {
                        $active = false;
                    }
                    $itemData['active'] = $active;
                    $itemData['flag'] = $active;

                    $newItem = $itemData;
                    $newItem['sub'] = $this->buildTree(
                        $itemObj->getChildren(),
                        $newItem['id'],
                        $language,
                        $baseurl,
                        $c,
                        $activeDocId
                    );
                    $result[] = $newItem;
                }
            }
        }

        if (\count($result) > 0) {
            return $result;
        }

        return null;
    }

    /**
     * Converts relative Content URLs to absolute URLs
     *
     * @param string $content
     * @param object $document
     * @param string $language
     * @param  string $categoryId
     *
     * @return string
     * @throws \Exception
     */
    private function convertUrls($content, $document, $language, $categoryId = null): string
    {
        $content = MailHelper::setAbsolutePaths($content, $document);

        $regexp = '/(<a [^>]+>)/i';

        \preg_match_all($regexp, $content, $aMatches);
        foreach ($aMatches[0] as $a) {
            if (\preg_match('/href="([^"]+)/', $a, $matches)) {
                $link = $matches[1];
                if (!\preg_match('/\.[a-z0-9A-Z]+$/', $link)) {
                    if (\strpos($link, \Pimcore\Tool::getHostUrl()) !== false) {
                        $newLink = \str_replace(
                            \Pimcore\Tool::getHostUrl() . '/' . $language,
                            '{SHOP_URL}',
                            \mb_strtolower($link)
                        );
                        if ($categoryId !== null && $categoryId > 0) {
                            $newLink .= '?c=' . $categoryId;
                        }
                        $newA = \str_replace($link, $newLink, $a);
                        $content = \str_replace($a, $newA, $content);
                    }
                }
            }
        }

        return $content;
    }

    /**
     * @param null $data
     * @param bool $wrapInDataProperty
     *
     * @return array
     */
    protected function createSuccessData($data = null, $wrapInDataProperty = true): array
    {
        if ($wrapInDataProperty) {
            $data = [
                'data' => $data
            ];
        }

        return array_merge(['success' => true], $this->normalizeResponseData($data));
    }

    /**
     * @param null $data
     *
     * @return array
     */
    protected function createErrorData($data = null): array
    {
        return array_merge(['success' => false], $this->normalizeResponseData($data));
    }

    /**
     * @param null $data
     *
     * @return array
     */
    protected function normalizeResponseData($data = null): ?array
    {
        if (null === $data) {
            $data = [];
        } elseif (is_string($data)) {
            $data = ['msg' => $data];
        }

        return $data;
    }

    /**
     * @param null $data
     * @param bool $wrapInDataProperty
     * @param int|null $status
     *
     * @return JsonResponse
     */
    protected function createSuccessResponse($data = null, $wrapInDataProperty = true, $status = Response::HTTP_OK): JsonResponse
    {
        return $this->adminJson(
            $this->createSuccessData($data, $wrapInDataProperty),
            $status
        );
    }

    /**
     * @param array $data
     * @param int   $status
     *
     * @return JsonResponse
     */
    protected function createCollectionSuccessResponse(array $data = [], $status = Response::HTTP_OK): JsonResponse
    {
        return $this->createSuccessResponse([
            'total' => count($data),
            'data' => $data,
        ], false, $status);
    }

    /**
     * Returns a JsonResponse that uses the admin serializer
     *
     * @param mixed $data    The response data
     * @param int $status    The status code to use for the Response
     * @param array $headers Array of extra headers to add
     * @param array $context Context to pass to serializer when using serializer component
     * @param bool $useAdminSerializer
     *
     * @return JsonResponse
     */
    protected function adminJson($data, $status = 200, $headers = [], $context = [], bool $useAdminSerializer = true): JsonResponse
    {
        $json = $this->encodeJson($data, $context, JsonResponse::DEFAULT_ENCODING_OPTIONS, $useAdminSerializer);

        return new JsonResponse($json, $status, $headers, true);
    }

    /**
     * Encodes data into JSON string
     *
     * @param mixed $data    The data to be encoded
     * @param array $context Context to pass to serializer when using serializer component
     * @param int $options   Options passed to json_encode
     * @param bool $useAdminSerializer
     *
     * @return string
     */
    protected function encodeJson($data, array $context = [], $options = JsonResponse::DEFAULT_ENCODING_OPTIONS, bool $useAdminSerializer = true): string
    {
        /** @var SerializerInterface $serializer */
        $serializer = null;

        if ($useAdminSerializer) {
            $serializer = $this->container->get('pimcore_admin.serializer');
        } else {
            $serializer = $this->container->get('serializer');
        }

        return $serializer->serialize($data, 'json', array_merge([
            'json_encode_options' => $options
        ], $context));
    }

    /**
     * @param null $data
     * @param int|null $status
     *
     * @return JsonResponse
     */
    protected function createErrorResponse($data = null, $status = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return $this->adminJson(
            $this->createErrorData($data),
            $status
        );
    }

    /**
     * @inheritDoc
     */
    protected function createNotFoundResponseException($message = null, \Exception $previous = null): ResponseException
    {
        return new ResponseException($this->createErrorResponse(
            $message ?: Response::$statusTexts[Response::HTTP_NOT_FOUND],
            Response::HTTP_NOT_FOUND
        ), $previous);
    }

    /**
     * Get decoded JSON request data
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws ResponseException
     */
    protected function getJsonData(Request $request): array
    {
        $data = null;
        $error = null;

        try {
            $data = $this->decodeJson($request->getContent());
        } catch (\Exception $e) {
            $this->getLogger()->error('Failed to decode JSON data for request {request}', [
                'request' => $request->getPathInfo()
            ]);

            $data = null;
            $error = $e->getMessage();
        }

        if (!is_array($data)) {
            $message = 'Invalid data';
            if (\Pimcore::inDebugMode()) {
                $message .= ': ' . $error;
            }

            throw new ResponseException($this->createErrorResponse([
                'msg' => $message
            ]));
        }

        return $data;
    }

    /**
     * Decodes a JSON string into an array/object
     *
     * @param mixed $json       The data to be decoded
     * @param bool $associative Whether to decode into associative array or object
     * @param array $context    Context to pass to serializer when using serializer component
     * @param bool $useAdminSerializer
     *
     * @return mixed
     */
    protected function decodeJson($json, $associative = true, array $context = [], bool $useAdminSerializer = true)
    {
        /** @var SerializerInterface|DecoderInterface $serializer */
        $serializer = null;

        if ($useAdminSerializer) {
            $serializer = $this->container->get('pimcore_admin.serializer');
        } else {
            $serializer = $this->container->get('serializer');
        }

        if ($associative) {
            $context['json_decode_associative'] = true;
        }

        return $serializer->decode($json, 'json', $context);
    }

    /**
     * Get ID either as parameter or from request
     *
     * @param Request $request
     * @param int|null $id
     *
     * @return mixed|null
     *
     * @throws ResponseException
     */
    protected function resolveId(Request $request, $id = null)
    {
        if (null !== $id) {
            return $id;
        }

        if ($id = $request->get('id')) {
            return $id;
        }

        throw new ResponseException($this->createErrorResponse('Missing ID'));
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger(): LoggerInterface
    {
        return $this->container->get('monolog.logger.pimcore_api');
    }

    /**
     * @param Request $request
     * @throws ResponseException
     */
    private function checkApiToken(Request $request): void
    {
        $apiKey = (string)$request->get('apikey');
        $apiKeyVerification = (string)$this->container->getParameter('restBundleApiKey');
        if ($apiKey === '') {
            throw new ResponseException($this->createErrorResponse(['Api key not provided'], Response::HTTP_UNAUTHORIZED));
        }
        if ($apiKeyVerification === '') {
            throw new ResponseException($this->createErrorResponse(['Api key not configured in parameters.yml'], Response::HTTP_UNAUTHORIZED));
        }
        if ($apiKey !== $apiKeyVerification) {
            throw new ResponseException($this->createErrorResponse(['Wrong api key provided'], Response::HTTP_UNAUTHORIZED));
        }

    }

}
